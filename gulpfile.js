const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat')
const sourcemaps = require('gulp-sourcemaps');
const watch = require('gulp-watch');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const browserSync = require('browser-sync').create;

const cssFiles =[
    './src/scss/font.scss',
    './src/scss/main.scss',
    './src/scss/mixins.scss',
    './src/scss/style.scss',
    './src/scss/variables.scss',
    './node_modules/normalize.css/normalize.css',
    './src/scss/block/header.scss',
    './src/scss/block/ceil.scss',
    './src/scss/block/banner.scss',
    './src/scss/block/guarantee.scss',
    './src/scss/templates/bl-container.scss'
];
const jsFiles = [

];
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        },
        browser: 'google chrome',
        notify: false
    });
});
gulp.task('sass-compile', function () {
    return gulp.src(cssFiles)
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('all.css'))
        .pipe(sourcemaps.init())
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(cleanCSS({level: 2}))
        .pipe(sourcemaps.write('./'))
        .pipe(browserSync.stream())
        .pipe(gulp.dest('src/build/css'))
})

gulp.task('js-compile', function () {
    return gulp.src(jsFiles)
        .pipe(concat('all.js'))
        .pipe(gulp.dest('./src/build/js'))
})

gulp.task('watch', function () {
    gulp.watch('src/scss/**/*.scss', gulp.series('sass-compile'))

})

